const webpack = require('webpack');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin')

const BUILD_DIR = path.resolve(__dirname, 'build');
const APP_DIR = path.resolve(__dirname, 'src');

let webConfig = () => {
    return {
        entry: path.join(APP_DIR, '/index.jsx'),
        output: {
            path: BUILD_DIR,
            filename: 'bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    include: APP_DIR,
                    use: 'babel-loader'
                },
                {
                    test: /\.less$/,
                    // include: APP_DIR,
                    use: [
                        { loader: 'style-loader' },
                        { loader: 'css-loader' },
                        {
                            loader: 'postcss-loader', options: {
                                plugins: () => {
                                    return [
                                        require('precss'),
                                        require('autoprefixer')
                                    ]
                                }

                            }
                        },
                        { loader: 'less-loader' }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                        { loader: 'style-loader' },
                        { loader: 'css-loader' },
                        {
                            loader: 'postcss-loader', options: {
                                plugins: () => {
                                    return [
                                        require('precss'),
                                        require('autoprefixer')
                                    ]
                                }

                            }
                        },
                        { loader: 'sass-loader' }
                    ]
                }

            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                React: 'react',
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Popper: ['popper.js', 'default'],
                PropTypes: 'prop-types'
            }),
            new HtmlWebpackPlugin({
                title: 'XYZ Dashboard'
            }),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin(require('config'))
        ],
        devServer: {
            contentBase: BUILD_DIR,
            hot: true
        }
    }
};


module.exports = webConfig;