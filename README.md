# Surepulse Dev Test #

Practical exam Dashboard UI application for fictional company XYZ. 

### How do I get set up? ###

* Install packages
```
npm i
```
* Run start command
```
npm run serve
```
* Then open your browser on this address
```
http://localhost:8080
```

### Directories ###

```
|---build                 build result files
|---config                application configuration (not much inside)
|---src                   Source code main folder
    |---navigation          navigation bar comp for navbar
    |---octicon             octicon comp for icons
    |---paginator           pagination comp
    |---sections            different view components (posts, comments, etc...)
    |---table               comp to show data
    
```