import Table from '../table/table.jsx';
import Paginator from '../paginator/paginator.jsx';
class Todos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            totalPages: 0,
            items: [],
            headers: [
                { dataHeader: 'id', headerDisplay: 'Id' },
                { dataHeader: 'userId', headerDisplay: 'User Id' },
                { dataHeader: 'title', headerDisplay: 'Title' },
                { dataHeader: 'completed', headerDisplay: 'Completed' }
            ]
        }
        this.getItems = this.getItems.bind(this)
    }
    componentDidMount() {
        this.getItems()
    }
    render() {
        return (
            <div>
                <Paginator
                    page={this.state.page}
                    totalPages={this.state.totalPages}
                    changePage={this.getItems.bind(this)}
                />
                <Table items={this.state.items} headers={this.state.headers} />
            </div>
        )
    }
    getItems(page = 1) {
        let totalPages = 0;
        fetch(`${XYZAPIBASEURL}/todos?_page=${page}`)
            .then(data => {
                totalPages = data.headers.get('X-Total-Count');
                return data.json()
            }, err => console.log(err))
            .then(data => {
                let transformedItems = data.map(x => {
                    return {
                        id: x.id,
                        userId: x.userId,
                        title: x.title,
                        completed: x.completed.toString()
                    }
                })
                this.setState({
                    page: Number(page),
                    items: transformedItems,
                    totalPages: Number(Math.ceil(totalPages / 10))
                });
            }, err => console.log(err))
    }
}

export default Todos;