import Table from '../table/table.jsx';
import Paginator from '../paginator/paginator.jsx';
class Comments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            totalPages: 0,
            items: [],
            headers: [
                { dataHeader: 'id', headerDisplay: 'Id' },
                { dataHeader: 'postId', headerDisplay: 'Post Id' },
                { dataHeader: 'name', headerDisplay: 'Name' },
                { dataHeader: 'email', headerDisplay: 'Email' },
                { dataHeader: 'body', headerDisplay: 'Body' }
            ]
        }
        this.getItems = this.getItems.bind(this)
    }
    componentDidMount() {
        this.getItems()
    }
    render() {
        return (
            <div>
                <Paginator
                    page={this.state.page}
                    totalPages={this.state.totalPages}
                    changePage={this.getItems.bind(this)}
                />
                <Table items={this.state.items} headers={this.state.headers} />
            </div>
        )
    }
    getItems(page = 1) {
        let totalPages = 0;
        fetch(`${XYZAPIBASEURL}/comments?_page=${page}`)
            .then(data => {
                totalPages = data.headers.get('X-Total-Count');
                return data.json()
            }, err => console.log(err))
            .then(data => {
                this.setState({
                    page: Number(page),
                    items: data,
                    totalPages: Number(Math.ceil(totalPages/10))
                });
            }, err => console.log(err))
    }
}

export default Comments;