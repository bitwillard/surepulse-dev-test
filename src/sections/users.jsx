import Table from '../table/table.jsx';
import Paginator from '../paginator/paginator.jsx';
class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            totalPages: 0,
            items: [],
            headers: [
                { dataHeader: 'id', headerDisplay: 'Id' },
                { dataHeader: 'name', headerDisplay: 'Name' },
                { dataHeader: 'username', headerDisplay: 'Username' },
                { dataHeader: 'email', headerDisplay: 'Email' },
                { dataHeader: 'address', headerDisplay: 'Address' },
                { dataHeader: 'phone', headerDisplay: 'Phone #' },
                { dataHeader: 'website', headerDisplay: 'Website' },
                { dataHeader: 'company', headerDisplay: 'Company' }
            ]
        }
        this.getItems = this.getItems.bind(this)
    }
    componentDidMount() {
        this.getItems()
    }
    render() {
        return (
            <div>
                <Paginator
                    page={this.state.page}
                    totalPages={this.state.totalPages}
                    changePage={this.getItems.bind(this)}
                />
                <Table items={this.state.items} headers={this.state.headers} />
            </div>
        )
    }
    getItems(page = 1) {
        let totalPages = 0;
        fetch(`${XYZAPIBASEURL}/users?_page=${page}`)
            .then(data => {
                totalPages = data.headers.get('X-Total-Count');
                return data.json()
            }, err => console.log(err))
            .then(data => {
                let transformedItems = data.map(x => {
                    let address = [
                        x.address.suite, 
                        x.address.street, 
                        x.address.city, 
                        x.address.zipcode, 
                        'lat:', x.address.geo.lat, 
                        'lng:', x.address.geo.lng
                    ].join(' ');
                    let company = [x.company.name, '\n',x.company.bs,'\n',x.company.catchPhrase]
                    return {
                        id: x.id,
                        name: x.name,
                        username: x.username,
                        email: x.email,
                        address: address,
                        phone: x.phone,
                        website: x.website,
                        company: company
                    }
                })
                this.setState({
                    page: Number(page),
                    items: transformedItems,
                    totalPages: Number(Math.ceil(totalPages / 10))
                });
            }, err => console.log(err))
    }
}

export default Users;


/**
 * {
          
          "company": {
            "name": "Hoeger LLC",
            "catchPhrase": "Centralized empowering task-force",
            "bs": "target end-to-end models"
          }
        }
 */