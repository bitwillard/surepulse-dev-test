import Table from '../table/table.jsx';
import Paginator from '../paginator/paginator.jsx';
class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            totalPages: 0,
            posts: [],
            headers: [
                { dataHeader: 'id', headerDisplay: 'Id' },
                { dataHeader: 'userId', headerDisplay: 'User Id' },
                { dataHeader: 'title', headerDisplay: 'Title' },
                { dataHeader: 'body', headerDisplay: 'Body' }
            ]
        }
        this.getPosts = this.getPosts.bind(this)
    }
    componentDidMount() {
        this.getPosts()
    }
    render() {
        return (
            <div>
                <Paginator
                    page={this.state.page}
                    totalPages={this.state.totalPages}
                    changePage={this.getPosts.bind(this)}
                />
                <Table items={this.state.posts} headers={this.state.headers} />
            </div>
        )
    }
    getPosts(page = 1) {
        let totalPages = 0;
        fetch(`${XYZAPIBASEURL}/posts?_page=${page}`)
            .then(data => {
                totalPages = data.headers.get('X-Total-Count');
                return data.json()
            }, err => console.log(err))
            .then(data => {
                this.setState({
                    page: Number(page),
                    posts: data,
                    totalPages: Number(Math.ceil(totalPages/10))
                });
            }, err => console.log(err))
    }
}

export default Posts;