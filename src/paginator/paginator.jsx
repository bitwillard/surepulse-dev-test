import './paginator.less'
import Octicon from '../octicon/octicon.jsx'
class Paginator extends React.Component {
    constructor(props) {
        super(props);
        this.next = this.next.bind(this)
        this.previous = this.previous.bind(this)
        this.first = this.first.bind(this)
        this.last = this.last.bind(this)
        this.pageExists = this.pageExists.bind(this)
    }
    render() {
        let choicesElement = this.makePageChoices(this.props.page, this.props.totalPages)
        return (
            <div className="container paginator">
                <div className="row">
                    <div className="col-md-6">
                        <div className="row">
                            <div className="col-auto">
                                <button type="button" onClick={this.first} className="btn btn-dark"
                                    disabled={this.props.page === 1}>
                                    {(<span><Octicon name='triangle-left' /><Octicon name='triangle-left' /></span>)}
                                </button>
                            </div>
                            <div className="col-auto">
                                <button type="button" onClick={this.previous} className="btn btn-dark"
                                    disabled={!this.pageExists(this.props.page - 1, this.props.totalPages)}>
                                    {<Octicon name='triangle-left' />}
                                </button>
                            </div>
                            <div className="col-auto">
                                <button type="button" onClick={this.next} className="btn btn-dark"
                                    disabled={!this.pageExists(this.props.page + 1, this.props.totalPages)}>
                                    {<Octicon name='triangle-right' />}
                                </button>
                            </div>
                            <div className="col-auto">
                                <button type="button" onClick={this.last} className="btn btn-dark"
                                    disabled={this.props.page === this.props.totalPages}>
                                    {(<span><Octicon name='triangle-right' /><Octicon name='triangle-right' /></span>)}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="row">
                            {choicesElement}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    next() {
        let newPage = this.props.page + 1;
        if (this.pageExists(newPage, this.props.totalPages))
            this.props.changePage(newPage);
    }
    previous() {
        let newPage = this.props.page - 1;
        if (this.pageExists(newPage, this.props.totalPages))
            this.props.changePage(newPage);
    }
    first() {
        this.props.changePage(1);
    }
    last() {
        this.props.changePage(this.props.totalPages);
    }
    jump(page) {
        if (this.pageExists(page, this.props.totalPages))
            this.props.changePage(page)
    }

    pageExists(page, totalPages) {
        return page >= 1 && page <= totalPages
    }

    makePageChoices(page, totalPages) {
        let choices = Array(7).fill(1).map((x, i) => i + page - 3)
        if (choices[0] > 1) choices.unshift('...')
        if (choices[choices.length - 1] < totalPages) choices.push('...')
        let choicesElement = choices.map((x, i) => {
            if (isNaN(x)) return <div key={`${x}-${i}`}>{x}</div>
            else if (this.pageExists(x, totalPages)) {
                let current = page === x;
                return (
                    <div className="col-auto" key={x}>
                        <button className="btn btn-primary"
                            disabled={current} onClick={this.jump.bind(this, x)}>
                            {x}
                        </button>
                    </div>)
            }
        })
        return choicesElement;
    }

}

Paginator.propTypes = {
    changePage: PropTypes.func,
    page: PropTypes.number,
    totalPages: PropTypes.number
}

export default Paginator;
