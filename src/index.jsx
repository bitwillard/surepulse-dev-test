import { render } from 'react-dom'
import {
  HashRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'
import './index.scss'
import './index.less'
import 'bootstrap'

import Navigation from './navigation/navigation.jsx';
import Table from './table/table.jsx'

import Posts from './sections/posts.jsx'
import Comments from './sections/comments.jsx'
import Albums from './sections/albums.jsx'
import Photos from './sections/photos.jsx'
import Todos from './sections/todos.jsx'
import Users from './sections/users.jsx'

class App extends React.Component {
  render() {
    let navs = [
      { label: 'Posts', link: '/posts', component: Posts },
      { label: 'Comments', link: '/comments', component: Comments },
      { label: 'Albums', link: '/albums', component: Albums },
      { label: 'Photos', link: '/photos', component: Photos },
      { label: 'Todos', link: '/todos', component: Todos },
      { label: 'Users', link: '/users', component: Users }
    ]
    let navRoutes = navs.map(x => (<Route exact path={x.link} component={x.component} key={x.label} />))
    let defaultRoute = (<Redirect to='/posts' />)
    return (
      <Router>
        <div>
          <Navigation navs={navs} />
          <div className="content container">
            <Switch>
              {navRoutes}
              {defaultRoute}
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}
let baseId = 'base'
let baseElement = document.createElement('div')
baseElement.id = baseId
document.body.appendChild(baseElement)
render(<App />, document.getElementById(baseId))