import React, { Component } from 'react';
import * as octicons from 'octicons';

class Octicon extends Component {
    render() {
        return (<span className="inverted" dangerouslySetInnerHTML={{ __html: octicons[this.props.name].toSVG() }}></span>)
    }
}
Octicon.propTypes = {
    name: PropTypes.string
}
export default Octicon;