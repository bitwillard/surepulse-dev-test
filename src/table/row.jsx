class Row extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let headers = this.props.headers;
        let post = this.props.post;
        let idHeader = headers[0].dataHeader
        let rowElements = headers.map((x, i) => {
            let key = x.dataHeader + post[idHeader];
            return (
                <td key={key}>{post[x.dataHeader]}</td>
            )
        })
        return (
            <tr key={this.props.post}>
                {rowElements}
            </tr>

        )
    }
}
Row.propTypes = {
    headers: PropTypes.array,
    post: PropTypes.object
}
export default Row;