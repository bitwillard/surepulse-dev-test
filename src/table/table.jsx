import Row from './row.jsx';
import Header from './header.jsx';
class Table extends React.Component {
    render() {
        let postRows = this.props.items.map(x => {
            let idHeader = this.props.headers[0].dataHeader
            let key = x[idHeader]
            return (
                <Row post={x} headers={this.props.headers} key={key} />
            )
        })
        let headerElement = <Header headers={this.props.headers} />;

        return (
            <div className="container">
                <table className="table table-light table-striped table-bordered">
                    <thead className="thead-dark">
                        {headerElement}
                    </thead>
                    <tbody className="">
                        {postRows}
                    </tbody>
                </table>
            </div>
        )
    }

}
Table.propTypes = {
    items: PropTypes.array,
    headers: PropTypes.array
}
export default Table;