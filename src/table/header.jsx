class Header extends React.Component {
    render() {
        let headers = this.props.headers;
        let headerElements = headers.map(x => {
            let key = x.dataHeader
            return (
                <th className="capitalize" key={key}>
                    {x.headerDisplay}
                </th>
            )
        }
        );
        return (
            < tr >
                {headerElements}
            </tr >
        )
    }
}
Header.propTypes = {
    headers: PropTypes.array
}

export default Header;