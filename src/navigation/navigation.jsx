import React from 'react'
import './navigation.less'

import NavItem from './navItem.jsx'

class Navigation extends React.Component {
    render() {
        let navs = this.props.navs

        let navItemElements = navs.map(x => <NavItem nav={x} key={x.label} />)
        return (
            <div className="side-nav">
                <div className="navbar navbar-expand-sm navbar-dark bg-dark">
                    <div className="navbar-brand">
                        <div className="container d-flex h-100">
                            <div className="row justify-content-center">
                                <div className="col align-self-center">
                                    <h1>XYZ</h1>
                                </div>

                            </div>
                        </div>
                    </div>
                    <button className="navbar-toggler" type='button'
                        data-toggle="collapse" data-target="#navlinks" >
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse side-navbar-collapse" id="navlinks">
                        <ul className="navbar-nav">
                            {navItemElements}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Navigation;