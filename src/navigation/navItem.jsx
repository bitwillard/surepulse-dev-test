import {
    NavLink
} from 'react-router-dom'

class NavItem extends React.Component {
    render() {
        return (
            <li className="nav-item">
                <NavLink to={this.props.nav.link} className='nav-link' activeClassName="active"><h3>{this.props.nav.label}</h3></NavLink>
            </li>
        )
    }
}
export default NavItem;